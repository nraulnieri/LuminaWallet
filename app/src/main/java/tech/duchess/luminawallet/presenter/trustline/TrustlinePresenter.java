package tech.duchess.luminawallet.presenter.trustline;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Patterns;

import com.moandjiezana.toml.Toml;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetCodeLengthInvalidException;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Operation;
import org.stellar.sdk.Transaction;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tech.duchess.luminawallet.dagger.SchedulerProvider;
import tech.duchess.luminawallet.model.api.HorizonApi;
import tech.duchess.luminawallet.model.fees.Fees;
import tech.duchess.luminawallet.model.persistence.account.Account;
import tech.duchess.luminawallet.model.persistence.account.AccountPrivateKey;
import tech.duchess.luminawallet.model.persistence.account.Balance;
import tech.duchess.luminawallet.model.repository.AccountRepository;
import tech.duchess.luminawallet.model.repository.FeesRepository;
import tech.duchess.luminawallet.model.util.AccountUtil;
import tech.duchess.luminawallet.model.util.AssetUtil;
import tech.duchess.luminawallet.model.util.FeesUtil;
import tech.duchess.luminawallet.model.util.SeedEncryptionUtil;
import tech.duchess.luminawallet.model.util.TransactionUtil;
import tech.duchess.luminawallet.presenter.common.BasePresenter;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.CommonTrustline;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.FederatedTrustlineChoice;
import tech.duchess.luminawallet.view.util.TextUtils;
import timber.log.Timber;

public class TrustlinePresenter extends BasePresenter<TrustlineContract.TrustlineView>
        implements TrustlineContract.TrustlinePresenter {
    private static final String URL_PREFIX = "https://";
    private static final String URL_SUFFIX_END = "stellar.toml";
    private static final String URL_SUFFIX_FULL = ".well-known/" + URL_SUFFIX_END;
    private static final Map<String, CommonTrustline> COMMON_TRUST_MAP;

    @NonNull
    private final OkHttpClient okHttpClient;

    @NonNull
    private final HorizonApi horizonApi;

    @NonNull
    private final AccountRepository accountRepository;

    @NonNull
    private final FeesRepository feesRepository;

    @NonNull
    private final SchedulerProvider schedulerProvider;

    @NonNull
    private final List<CommonTrustline> remainingCommonTrusts = new ArrayList<>();

    private String accountId;

    static {
        COMMON_TRUST_MAP = new HashMap<>();
        for (CommonTrustline trustline : CommonTrustline.values()) {
            COMMON_TRUST_MAP.put(trustline.getIssuer() + trustline.getAssetCode(), trustline);
        }
    }

    TrustlinePresenter(@NonNull TrustlineContract.TrustlineView view,
                       @NonNull OkHttpClient okHttpClient,
                       @NonNull HorizonApi horizonApi,
                       @NonNull AccountRepository accountRepository,
                       @NonNull FeesRepository feesRepository,
                       @NonNull SchedulerProvider schedulerProvider) {
        super(view);
        this.okHttpClient = okHttpClient;
        this.horizonApi = horizonApi;
        this.accountRepository = accountRepository;
        this.feesRepository = feesRepository;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void start(@Nullable Bundle bundle) {
        super.start(bundle);
        accountId = view.getAccountId();
        refreshTrustlines(false);
    }

    @SuppressLint("CheckResult")
    private void refreshTrustlines(boolean deepPull) {
        accountRepository.getAccountById(accountId, deepPull)
                .compose(schedulerProvider.singleScheduler())
                .flatMap(this::getTrustlines)
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoading(true);
                })
                .doAfterTerminate(() -> view.showLoading(false))
                .subscribe(view::showTrustlines);
    }

    private Single<List<TrustlineContract.Trustline>> getTrustlines(@NonNull Account account) {
        return Observable.fromIterable(account.getBalances())
                .doOnSubscribe(disposable -> {
                    remainingCommonTrusts.clear();
                    remainingCommonTrusts.addAll(Arrays.asList(CommonTrustline.values()));
                })
                .filter(balance -> !AssetUtil.isLumenBalance(balance))
                .sorted((o1, o2) -> o1.getAsset_issuer().compareTo(o2.getAsset_issuer()))
                .map(balance -> {
                    String issuer = balance.getAsset_issuer();
                    String assetCode = balance.getAsset_code();
                    String commonKey = issuer + assetCode;
                    if (COMMON_TRUST_MAP.containsKey(commonKey)) {
                        remainingCommonTrusts.remove(COMMON_TRUST_MAP.get(commonKey));
                        return (TrustlineContract.Trustline) new TrustBalance(COMMON_TRUST_MAP.get(commonKey), balance);
                    } else {
                        return new TrustBalance(balance);
                    }
                })
                .toList();
    }

    @Override
    public void onUserRequestRefresh() {
        refreshTrustlines(true);
    }

    @Override
    public void onUserRequestRemoveTrust(@NonNull TrustlineContract.Trustline trustline) {
        getFeesForTransactionConfirmation(fees ->
                view.showRemoveTrustlineConfirmation(trustline, getSingleTransactionFee(fees)));
    }

    @SuppressLint("CheckResult")
    @Override
    public void onUserRequestRemoveTrust(@NonNull TrustlineContract.Trustline trustline,
                                         @Nullable String password) {
        if (!SeedEncryptionUtil.checkPasswordLength(password)) {
            view.showRemoveTrustlineError(TrustlineRemoveError.PASSWORD_LENGTH);
            return;
        }

        Single.zip(accountRepository.getAccountById(accountId, true),
                accountRepository.getEncryptedSeed(accountId),
                (Pair::new))
                .compose(schedulerProvider.singleScheduler())
                .flatMapCompletable(pair -> {
                    Account account = pair.first;
                    Double curAmount = Observable.fromIterable(account.getBalances())
                            .filter(balance -> balance.getAsset_code().equals(trustline.getAssetCode()))
                            .filter(balance -> balance.getAsset_issuer().equals(trustline.getIssuer()))
                            .map(Balance::getBalance)
                            .firstOrError()
                            .blockingGet();

                    if (curAmount > 0) {
                        throw new BalanceNotZeroException();
                    }

                    AccountPrivateKey accountPrivateKey = pair.second;
                    Asset asset = Asset.createNonNativeAsset(trustline.getAssetCode(),
                            KeyPair.fromAccountId(trustline.getIssuer()));
                    KeyPair signer = KeyPair.fromSecretSeed(SeedEncryptionUtil
                            .decryptSeed(accountPrivateKey.getEncryptedSeedPackage(), password));
                    Transaction trustTransaction =
                            new Transaction.Builder(account)
                                    .addOperation(getTrustOperation(accountId, asset, "0"))
                                    .build();
                    trustTransaction.sign(signer);

                    return horizonApi.postTransaction(
                            TransactionUtil.getEnvelopeXDRBase64(trustTransaction))
                            .compose(schedulerProvider.completableScheduler());
                })
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showBlockedLoading(false);
                })
                .doAfterTerminate(() -> refreshTrustlines(true))
                .subscribe(() ->
                                view.hideBlockedLoading(true, false),
                        throwable -> {
                            view.hideBlockedLoading(false, false);
                            if (throwable instanceof BalanceNotZeroException) {
                                view.showRemoveTrustlineError(TrustlineRemoveError.BALANCE_NOT_ZERO);
                            } else {
                                Timber.e(throwable, "Remove trustline transaction failed");
                            }
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void onUserRequestAddCommonTrust() {
        if (remainingCommonTrusts.isEmpty()) {
            view.showNoCommonTrustsLeft();
        } else {
            getFeesForTransactionConfirmation(fees ->
                    view.showCommonTrustOptions(remainingCommonTrusts,
                            getSingleTransactionFee(fees)));
        }
    }

    @Override
    public void onUserRequestManualInput() {
        getFeesForTransactionConfirmation(fees ->
                view.showManualInputConfirmation(getSingleTransactionFee(fees)));
    }

    @SuppressLint("CheckResult")
    private void getFeesForTransactionConfirmation(@NonNull Consumer<Fees> feesConsumer) {
        feesRepository.getFees(false)
                .compose(schedulerProvider.singleScheduler())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoading(true);
                })
                .doAfterTerminate(() -> {
                    view.showLoading(false);
                })
                .subscribe(feesConsumer,
                        throwable -> {
                            Timber.e(throwable, "Failed to get fees");
                            view.showTrustlineRequestError(TrustlineRequestError.UNKNOWN_ERROR);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void onUserInputFederationUrl(@Nullable String federationUrl) {
        if (federationUrl == null || !Patterns.WEB_URL.matcher(federationUrl).matches()) {
            view.showFederationUrlError(federationUrl);
            return;
        }

        URI uri;
        try {
            uri = new URI(federationUrl).normalize();
        } catch (URISyntaxException e) {
            Timber.e(e, "Failed to construct federation uri");
            view.showFederationUrlError(federationUrl);
            return;
        }

        parseFederationUrl(uri)
                .compose(schedulerProvider.observableScheduler())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoading(true);
                })
                .doAfterTerminate(() -> view.showLoading(false))
                .map(response -> new Toml().read(response.body().byteStream()))
                .map(toml -> parseCurrencyToml(toml.getTables("CURRENCIES")))
                .subscribe(trustlineChoices -> {
                    if (trustlineChoices.size() == 1) {
                        // We can immediately go to trustline input.
                        getFeesForTransactionConfirmation(fees -> {
                            view.showTrustlineInputDialog(trustlineChoices.get(0),
                                    getSingleTransactionFee(fees));
                        });
                    } else {
                        view.showFederatedTrustlineOptions(trustlineChoices);
                    }
                }, throwable -> {
                    Timber.e(throwable, "Failed to parse federation toml");
                    view.showFederationUrlError(federationUrl);
                });
    }

    private static double getSingleTransactionFee(@NonNull Fees fees) {
        return FeesUtil.getTransactionFee(fees, 1);
    }

    private List<FederatedTrustlineChoice> parseCurrencyToml(@Nullable List<Toml> currencies) {
        if (currencies == null) {
            throw new IllegalArgumentException("No currencies defined");
        }

        List<FederatedTrustlineChoice> choices = new ArrayList<>();
        for (Toml toml : currencies) {
            String code = toml.getString("code");
            String name = toml.getString("name");
            String issuer = toml.getString("issuer");

            if (TextUtils.isEmpty(code) || TextUtils.isEmpty(name) || TextUtils.isEmpty(issuer)) {
                continue;
            }

            choices.add(new FederatedTrustlineChoice(code, name, issuer));
        }

        if (choices.isEmpty()) {
            throw new IllegalArgumentException("No currency definitions valid");
        }

        return choices;
    }

    private Observable<Response> parseFederationUrl(@NonNull URI uri) {
        return Observable.fromCallable(() ->
                okHttpClient.newCall(new Request.Builder().url(sanitizeUri(uri))
                        .addHeader("Accept", "text/*")
                        .build())
                        .execute())
                .map(response -> {
                    if (!response.isSuccessful()) {
                        throw new Exception("Failed to download federation toml");
                    }

                    return response;
                });
    }

    private String sanitizeUri(@NonNull URI uri) {
        String uriString = uri.toString().toLowerCase();
        StringBuilder stringBuilder = new StringBuilder(uriString);
        if (!uriString.startsWith(URL_PREFIX)) {
            stringBuilder.insert(0, URL_PREFIX);
        }

        if (!uriString.endsWith(URL_SUFFIX_END)) {
            if (!uriString.endsWith("/")) {
                stringBuilder.append("/");
            }

            stringBuilder.append(URL_SUFFIX_FULL);
        }

        return stringBuilder.toString();
    }

    @SuppressLint("CheckResult")
    @Override
    public void onUserRequestAddTrust(@Nullable String issuer,
                                      @Nullable String assetCode,
                                      @Nullable String limit,
                                      @Nullable String password) {
        final String checkedLimit = TextUtils.isEmpty(limit) ?
                AssetUtil.MAX_ASSET_STRING_VALUE
                : limit;
        Asset asset;
        try {
            TrustlineRequestError error = null;
            if (!AccountUtil.publicKeyOfProperLength(issuer)) {
                error = TrustlineRequestError.ISSUER_ADDRESS_LENGTH;
            } else if (!AccountUtil.publicKeyOfProperPrefix(issuer)) {
                error = TrustlineRequestError.ISSUER_ADDRESS_PREFIX;
            } else if (!AccountUtil.publicKeyCanBeDecoded(issuer)) {
                error = TrustlineRequestError.ISSUER_ADDRESS_FORMAT;
            } else if (!SeedEncryptionUtil.checkPasswordLength(password)) {
                error = TrustlineRequestError.PASSWORD_LENGTH;
            } else if (!isTrustLimitValid(checkedLimit)) {
                error = TrustlineRequestError.TRUST_AMOUNT_INVALID;
            } else if (assetCode == null) {
                error = TrustlineRequestError.ASSET_CODE_INVALID;
            }

            if (error != null) {
                view.showTrustlineRequestError(error);
                return;
            }

            try {
                asset = Asset.createNonNativeAsset(assetCode, KeyPair.fromAccountId(issuer));
            } catch (AssetCodeLengthInvalidException e) {
                view.showTrustlineRequestError(TrustlineRequestError.ASSET_CODE_INVALID);
                return;
            }
        } catch (Exception e) {
            Timber.e(e, "Exception creating trustline asset");
            view.showTrustlineRequestError(TrustlineRequestError.UNKNOWN_ERROR);
            return;
        }

        Single.zip(accountRepository.getAccountById(accountId, true),
                accountRepository.getEncryptedSeed(accountId),
                feesRepository.getFees(false),
                (TrustlineRequestPackage::new))
                .compose(schedulerProvider.singleScheduler())
                .flatMapCompletable(trustlineRequestPackage -> {
                    Account account = trustlineRequestPackage.account;
                    checkSufficientFundsForNewTrustline(account, trustlineRequestPackage.fees);
                    AccountPrivateKey accountPrivateKey = trustlineRequestPackage.accountPrivateKey;
                    KeyPair signer = KeyPair.fromSecretSeed(SeedEncryptionUtil
                            .decryptSeed(accountPrivateKey.getEncryptedSeedPackage(), password));
                    Transaction trustTransaction =
                            new Transaction.Builder(account)
                                    .addOperation(getTrustOperation(accountId, asset, checkedLimit))
                                    .build();
                    trustTransaction.sign(signer);

                    return horizonApi.postTransaction(
                            TransactionUtil.getEnvelopeXDRBase64(trustTransaction))
                            .compose(schedulerProvider.completableScheduler());
                })
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showBlockedLoading(true);
                })
                .doAfterTerminate(() -> refreshTrustlines(true))
                .subscribe(() ->
                                view.hideBlockedLoading(true, true),
                        throwable -> {
                            view.hideBlockedLoading(false, true);
                            if (throwable instanceof InsufficientFundsException) {
                                view.showInsufficientFundsError(
                                        ((InsufficientFundsException) throwable).minimumBalance);
                            } else {
                                Timber.e(throwable, "Trustline transaction failed");
                            }
                        });
    }

    private void checkSufficientFundsForNewTrustline(@NonNull Account account,
                                                     @NonNull Fees fees)
            throws InsufficientFundsException {
        double curMinimumBalance = FeesUtil.getMinimumAccountBalance(fees, account);
        double transactionFee = FeesUtil.getTransactionFee(fees, 1);
        double lumenBalance = account.getLumens().getBalance();

        // Adding a trust: Current Minimum Balance + Transaction Fee
        // + Base Reserve (for new Trustline entry)
        double newMinimumBalance = curMinimumBalance + transactionFee
                + Double.valueOf(fees.getBase_reserve());

        if (lumenBalance < newMinimumBalance) {
            throw new InsufficientFundsException(newMinimumBalance);
        }
    }

    private class InsufficientFundsException extends Exception {
        final double minimumBalance;
        InsufficientFundsException(double minimumBalance) {
            this.minimumBalance = minimumBalance;
        }
    }

    private class BalanceNotZeroException extends Exception {
    }

    private static class TrustlineRequestPackage {
        @NonNull
        public final Account account;

        @NonNull
        public final AccountPrivateKey accountPrivateKey;

        @NonNull
        public final Fees fees;

        private TrustlineRequestPackage(@NonNull Account account,
                                        @NonNull AccountPrivateKey accountPrivateKey,
                                        @NonNull Fees fees) {
            this.account = account;
            this.accountPrivateKey = accountPrivateKey;
            this.fees = fees;
        }
    }

    private boolean isTrustLimitValid(@Nullable String limit) {
        if (TextUtils.isEmpty(limit)) {
            return false;
        }

        double trustLimit = Double.valueOf(limit);
        return trustLimit > 0 && trustLimit <= AssetUtil.MAX_ASSET_VALUE;
    }

    private Operation getTrustOperation(@NonNull String accountId,
                                        @NonNull Asset asset,
                                        @NonNull String limit) {
        return new ChangeTrustOperation.Builder(asset, limit)
                .setSourceAccount(KeyPair.fromAccountId(accountId))
                .build();
    }

    @Override
    public void onUserSelectedFederatedTrustline(@NonNull FederatedTrustlineChoice trustlineChoice) {
        getFeesForTransactionConfirmation(fees ->
                view.showTrustlineInputDialog(trustlineChoice, getSingleTransactionFee(fees)));
    }

    private class TrustBalance implements TrustlineContract.Trustline {
        @NonNull
        private final Balance balance;

        @Nullable
        private final CommonTrustline commonTrustline;

        TrustBalance(@NonNull Balance balance) {
            this(null, balance);
        }

        TrustBalance(@Nullable CommonTrustline trustline, @NonNull Balance balance) {
            this.commonTrustline = trustline;
            this.balance = balance;
        }

        @NonNull
        @Override
        public String getIssuer() {
            return balance.getAsset_issuer();
        }

        @Override
        public int getColor() {
            return commonTrustline == null ? -1 : commonTrustline.getColor();
        }

        @Nullable
        @Override
        public Integer getLogoResId() {
            return commonTrustline == null ? null : commonTrustline.logoResId;
        }

        @NonNull
        @Override
        public String getAssetCode() {
            return balance.getAsset_code();
        }

        @Nullable
        @Override
        public String getUrl() {
            return commonTrustline == null ? null : commonTrustline.getUrl();
        }

        @NonNull
        @Override
        public String getLimitText() {
            return AssetUtil.getAssetAmountString(balance.getLimit());
        }

        @Override
        public double getLimit() {
            return balance.getLimit();
        }
    }
}
