package tech.duchess.luminawallet.view.common;

public interface BackPressListener {
    void onBackPressed();
}
