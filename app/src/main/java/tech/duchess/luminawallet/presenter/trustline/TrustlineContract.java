package tech.duchess.luminawallet.presenter.trustline;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import tech.duchess.luminawallet.R;
import tech.duchess.luminawallet.model.util.AssetUtil;
import tech.duchess.luminawallet.presenter.common.Presenter;

public interface TrustlineContract {
    interface TrustlineView {
        @Nullable
        String getAccountId();

        void showLoading(boolean isLoading);

        void showBlockedLoading(boolean isAddingTrust);

        void hideBlockedLoading(boolean wasSuccess, boolean isAddingTrust);

        void showTrustlines(@NonNull List<Trustline> currentTrusts);

        void showCommonTrustOptions(@NonNull List<CommonTrustline> commonTrustlines, double fee);

        void showManualInputConfirmation(double fee);

        void showNoCommonTrustsLeft();

        void showFederationUrlError(@Nullable String url);

        void showFederatedTrustlineOptions(@NonNull List<FederatedTrustlineChoice> trustlines);

        void showTrustlineInputDialog(@NonNull FederatedTrustlineChoice trustlineChoice, double fee);

        void showInsufficientFundsError(double minimumBalance);

        void showTrustlineRequestError(@NonNull TrustlinePresenter.TrustlineRequestError error);

        void showRemoveTrustlineError(@NonNull TrustlinePresenter.TrustlineRemoveError trustlineRemoveError);

        void showRemoveTrustlineConfirmation(@NonNull Trustline trustline, double fee);

        void showRefreshFailedError();
    }

    interface TrustlinePresenter extends Presenter {

        enum TrustlineRequestError {
            ISSUER_ADDRESS_LENGTH,
            ISSUER_ADDRESS_PREFIX,
            ISSUER_ADDRESS_FORMAT,
            TRUST_AMOUNT_INVALID,
            ASSET_CODE_INVALID,
            PASSWORD_LENGTH,
            UNKNOWN_ERROR
        }

        enum TrustlineRemoveError {
            PASSWORD_LENGTH,
            BALANCE_NOT_ZERO
        }

        void onUserRequestRefresh();

        void onUserRequestRemoveTrust(@NonNull Trustline trustline);

        void onUserRequestRemoveTrust(@NonNull Trustline trustline,
                                      @NonNull String password);

        void onUserRequestAddCommonTrust();

        void onUserRequestManualInput();

        void onUserInputFederationUrl(@Nullable String federationUrl);

        void onUserSelectedFederatedTrustline(@NonNull FederatedTrustlineChoice trustlineChoice);

        void onUserRequestAddTrust(@Nullable String issuer,
                                   @Nullable String assetCode,
                                   @Nullable String limit,
                                   @Nullable String password);
    }

    // TODO: Maybe add description and conditions for user visibility?
    class FederatedTrustlineChoice implements Parcelable {
        @NonNull
        public final String assetCode;

        @NonNull
        public final String assetName;

        @NonNull
        public final String assetIssuer;

        public FederatedTrustlineChoice(@NonNull String assetCode,
                                        @NonNull String assetName,
                                        @NonNull String assetIssuer) {
            this.assetCode = assetCode;
            this.assetName = assetName;
            this.assetIssuer = assetIssuer;
        }

        protected FederatedTrustlineChoice(Parcel in) {
            assetCode = in.readString();
            assetName = in.readString();
            assetIssuer = in.readString();
        }

        public static final Creator<FederatedTrustlineChoice> CREATOR = new Creator<FederatedTrustlineChoice>() {
            @Override
            public FederatedTrustlineChoice createFromParcel(Parcel in) {
                return new FederatedTrustlineChoice(in);
            }

            @Override
            public FederatedTrustlineChoice[] newArray(int size) {
                return new FederatedTrustlineChoice[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(assetCode);
            dest.writeString(assetName);
            dest.writeString(assetIssuer);
        }
    }

    interface Trustline {
        @NonNull
        String getIssuer();

        @ColorInt
        int getColor();

        @DrawableRes
        @Nullable
        Integer getLogoResId();

        @NonNull
        String getAssetCode();

        @Nullable
        String getUrl();

        @NonNull
        String getLimitText();

        double getLimit();
    }

    // TODO: This is a bit risky, being that these are hardcoded. So these should be maintained
    // both carefully and frequently. Perhaps we can do some sort of latest-version check here
    // to warn users against potentially stale data.
    enum CommonTrustline implements Trustline {
        IRENE_ENERGY(
                "GBBRMEXJMS3L7Y3DZZ2AHBD545GZ72OAEHHEFKGZAHHASHGWMHK5P6PL",
                0xFF38909A,
                R.drawable.irene_energy_logo,
                "TELLUS",
                "https://irene.energy",
                510000000.0),
        MOBIUS(
                "GA6HCMBLTZS5VYYBCATRBRZ3BZJMAFUDKYYF6AH6MVCMGWMRDNSWJPIH",
                0xFF6F39FC,
                R.drawable.mobius_logo,
                "MOBI",
                "https://mobius.network",
                888000000.0),
        PEDITY(
                "GBVUDZLMHTLMZANLZB6P4S4RYF52MVWTYVYXTQ2EJBPBX4DZI2SDOLLY",
                0xFF2A8BC6,
                R.drawable.pedity_logo,
                "PEDI",
                "https://pedity.com",
                3999999999.0),
        REPO_COIN(
                "GCZNF24HPMYTV6NOEHI7Q5RJFFUI23JKUKY3H3XTQAFBQIBOHD5OXG3B",
                0xFF192854,
                R.drawable.repocoin_logo,
                "REPO",
                "https://repocoin.io",
                357000000.0),
        SMARTLANDS(
                "GCKA6K5PCQ6PNF5RQBF7PQDJWRHO6UOGFMRLK3DYHDOI244V47XKQ4GP",
                0xFF35AB62,
                R.drawable.smartlands_logo,
                "SLT",
                "https://smartlands.io",
                7186241.0),
        SUREREMIT(
                "GDEGOXPCHXWFYY234D2YZSPEJ24BX42ESJNVHY5H7TWWQSYRN5ZKZE3N",
                0xFF363187,
                R.drawable.sureremit_logo,
                "RMT",
                "https://sureremit.co",
                1000000000.0),
        TEMPO(
                "GAP5LETOV6YIE62YAM56STDANPRDO7ZFDBGSNHJQIYGGKSMOZAHOOS2S",
                0xFF1B92DE,
                R.drawable.tempo_logo,
                "EURT",
                "https://tempo.eu.com",
                AssetUtil.MAX_ASSET_VALUE),
        XIM(
                "GBZ35ZJRIKJGYH5PBKLKOZ5L6EXCNTO7BKIL7DAVVDFQ2ODJEEHHJXIM",
                0xFF000000,
                R.drawable.xim_logo,
                "XIM",
                "https://ximcoin.com",
                10000000000000.0);

        @NonNull
        String issuer;
        @ColorInt
        int color;
        @DrawableRes
        int logoResId;
        @NonNull
        String assetCode;
        @NonNull
        String url;

        double limit;


        CommonTrustline(@NonNull String issuer,
                        @ColorInt int color,
                        @DrawableRes int logoResId,
                        @NonNull String assetCode,
                        @NonNull String url,
                        double limit) {
            this.issuer = issuer;
            this.color = color;
            this.logoResId = logoResId;
            this.assetCode = assetCode;
            this.url = url;
            this.limit = limit;
        }

        @NonNull
        @Override
        public String getIssuer() {
            return issuer;
        }

        @Override
        public int getColor() {
            return color;
        }

        @NonNull
        @Override
        public Integer getLogoResId() {
            return logoResId;
        }

        @NonNull
        @Override
        public String getAssetCode() {
            return assetCode;
        }

        @NonNull
        @Override
        public String getUrl() {
            return url;
        }

        @NonNull
        @Override
        public String getLimitText() {
            return AssetUtil.getAssetAmountString(limit);
        }

        @Override
        public double getLimit() {
            return limit;
        }
    }
}
